
const initialState = {
  email: '',
}
  
export const registration = (state=initialState, action) => {
  switch (action.type) {
    case "EMAIL_REGISTER": {
      return {
        ...state,
        email: action.email
      }
    }
    default: {
      return state
    }
  }
}