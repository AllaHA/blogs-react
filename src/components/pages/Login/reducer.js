const initialState = {
  loggedUser: {}
}

export const login = (state=initialState, action) => {
  switch (action.type) {
    case "GET_LOGGED_USER": {
      return {
        ...state,
        loggedUser: action.loggedUser
      }
    }
    default: {
      return state
    }
  }
}